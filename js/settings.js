module.exports = {
  adapterResponseSize: 20,
  adapterRequestsInterval: 300,
  playerLogInterval: 5000
}