var Player = require("./Player.js");

VK.init(function() {
  VK.callMethod("showSettingsBox", 16);
}, function() { 
  console.log(arguments)
}, '5.27'); 

document.addEventListener("DOMContentLoaded", function() {
  var container = document.getElementById("container");

  new Player(container);
});