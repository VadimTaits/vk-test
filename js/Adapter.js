var videosMask = require("./consts.js").videosMask,
    settings = require("./settings.js");

var Adapter = function(onSettingsChanged) {
  VK.addCallback("onSettingsChanged", function(settings) {
    if (typeof onSettingsChanged === "function") {
      onSettingsChanged(!!(settings & videosMask));
    }
  });
}

Adapter.prototype.processResponse = function(videos, offset) {
  var testEl = document.createElement( "video" );

  return videos.map(function(item, i) {
    item.number = offset + i;

    return item;
  }).filter(function(item) {
    if (!item.files) {
      return false;
    }

    if (item.files.external) {
      return false;
    }

    var foundedLink = false;

    for (var key in item.files) {
      if (item.files.hasOwnProperty(key) && testEl.canPlayType("video/" + key.split("_")[0])) {
        return true;
      }
    }

    return false;
  });
}

Adapter.prototype.getFirstVideo = function(sCb, fCb, offset) {
  this.getVideos(1, function(data) {
    if (!data.status || data.videos.length === 0) {
      sCb({
        status: false
      })

      return;
    }

    sCb({
      status: true,
      video: data.videos[0]
    });
  }, fCb, offset);
}

Adapter.prototype.getVideos = function(number, sCb, fCb, offset, memo) {
  if (!offset) {
    offset = 0;
  }

  if (!memo) {
    memo = [];
  }

  VK.api("video.get", {
    offset: offset,
    count: settings.adapterResponseSize
  }, function(data) {
    if (data.error) {
      fCb();

      return;
    }

    var videos = this.processResponse(data.response.items, offset);

    if (memo.length + videos.length >= number) {
      sCb({
        status: true,
        videos: memo.concat(videos.slice(0, number - memo.length))
      });

      return;
    }

    if (data.response.count > offset + settings.adapterResponseSize) {
      setTimeout(function() {
        this.getVideos(number, sCb, fCb, offset + settings.adapterResponseSize, memo.concat(videos));
      }.bind(this), settings.adapterRequestsInterval);

      return;
    }

    sCb({
      status: true,
      videos: memo.concat(videos)
    });
  }.bind(this));
}

Adapter.prototype.getVideo = function(number, sCb, fCb) {
  VK.api("video.get", {
    offset: number,
    count: 1
  }, function(data) {
    if (data.error) {
      fCb();

      return;
    }

    if (data.response.items.length === 0) {
      sCb({
        status: false
      });

      return;
    }

    sCb({
      status: true,
      video: data.response.items[0]
    });
  });
}

module.exports = Adapter;