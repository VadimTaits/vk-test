/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var Player = __webpack_require__(1);

	VK.init(function() {
	  VK.callMethod("showSettingsBox", 16);
	}, function() { 
	  console.log(arguments)
	}, '5.27'); 

	document.addEventListener("DOMContentLoaded", function() {
	  var container = document.getElementById("container");

	  new Player(container);
	});

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var Adapter = __webpack_require__(2),
	    settings = __webpack_require__(3)

	var Player = function(el) {
	  this.el = el;
	  this.previewsEl = this.el.querySelector("[data-previews]");

	  this.initVideoEl();

	  this.adapter = new Adapter(this.onSettingsChanged.bind(this));
	}

	Player.prototype.initVideoEl = function() {
	  this.videoEl = this.el.querySelector("[data-video]");

	  this.videoEl.onended = function() {
	    this.setState("show-more");
	  }.bind(this);

	  this.videoEl.onpause = function() {
	    this.clearLogInterval();
	  }.bind(this);

	  this.videoEl.onplaying = function() {
	    this.setLogInterval();
	  }.bind(this);
	}

	Player.prototype.setLogInterval = function(e) {
	  this.logInterval = setInterval(function() {
	    var currentTime = Math.floor(this.videoEl.currentTime);

	    if (currentTime > this.counter) {
	      this.counter = currentTime;
	    }

	    if (window.console) {
	      console.log("Просмотрено " + this.counter + " секунд ролика");
	    }
	  }.bind(this), settings.playerLogInterval);
	}

	Player.prototype.clearLogInterval = function(e) {
	  if (this.logInterval) {
	    clearInterval(this.logInterval);
	  }
	}

	Player.prototype.accessError = function() {
	  this.error("Пожалуйста, разрешите доступ к Вашим видеозаписям");
	}

	Player.prototype.videoNotFoundError = function() {
	  this.error("Пожалуйста, добавьте хотя бы одну видеозапись, формат которой поддерживается Вашим браузером");
	}

	Player.prototype.onSettingsChanged = function(allowedVideos) {
	  if (allowedVideos) {
	    this.adapter.getFirstVideo(function(data) {
	      if (!data.status) {
	        this.videoNotFoundError();
	        return;
	      }

	      this.video = data.video;

	      this.setState("play");
	    }.bind(this), this.accessError.bind(this));
	  } else {
	    this.accessError();
	  }
	}

	Player.prototype.setState = function(state) {
	  Array.prototype.forEach.call(this.el.querySelectorAll("[data-state]"), function(el) {
	    var elState = el.attributes.getNamedItem("data-state").value;

	    if (elState === state) {
	      el.style.display = "block";
	    } else {
	      el.style.display = "none"
	    }
	  });

	  switch (state) {
	    case "play":
	      var url = this.getVideoLink(this.video);
	      this.counter = 0;
	      this.playVideo(url);

	      break;

	    case "show-more":
	      this.loadPreview(this.video.number + 1);
	      break;

	    default:
	      break;
	  }
	}

	Player.prototype.loadPreview = function(offset) {
	  this.previewsEl.innerHTML = '<div class="preloader"></div>';

	  this.adapter.getVideos(16, function(data) {
	    if (!data.status) {
	      this.videoNotFoundError();
	      return;
	    }

	    if (data.videos.length > 0) {
	      this.makePreview(data.videos);
	      return;
	    }

	    if (offset === 0) {
	      this.videoNotFoundError();
	      return;
	    }

	    this.loadPreview(0);
	  }.bind(this), this.accessError.bind(this), offset);
	}

	Player.prototype.makePreview = function(videos) {
	  this.previewsEl.innerHTML = "";

	  videos.forEach(function(item, i) {
	    var el = document.createElement("div");

	    if (el.classList) {
	      el.classList.add("preview");
	    } else {
	      el.className = "preview";
	    }

	    el.style.backgroundImage = "url('" + item.photo_320 + "')";

	    el.onclick = function() {
	      this.video = videos[i];

	      this.setState("play");
	    }.bind(this);

	    this.previewsEl.appendChild(el);
	  }.bind(this));
	}

	Player.prototype.playVideo = function(url) {
	  this.videoEl.src = url;
	  this.videoEl.play();
	}

	Player.prototype.getVideoLink = function(item) {
	  var bestQuality;

	  for (var key in item.files) {
	    if (item.files.hasOwnProperty(key) && item.files[key]) {
	      bestQuality = item.files[key];
	    }
	  }

	  return bestQuality;
	}

	Player.prototype.error = function(message) {
	  var errorEl = this.el.querySelector("[data-error-message]");
	  errorEl.innerText = message;
	  errorEl.textContent = message;

	  this.setState("error");
	}

	module.exports = Player;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var videosMask = __webpack_require__(4).videosMask,
	    settings = __webpack_require__(3);

	var Adapter = function(onSettingsChanged) {
	  VK.addCallback("onSettingsChanged", function(settings) {
	    if (typeof onSettingsChanged === "function") {
	      onSettingsChanged(!!(settings & videosMask));
	    }
	  });
	}

	Adapter.prototype.processResponse = function(videos, offset) {
	  var testEl = document.createElement( "video" );

	  return videos.map(function(item, i) {
	    item.number = offset + i;

	    return item;
	  }).filter(function(item) {
	    if (!item.files) {
	      return false;
	    }

	    if (item.files.external) {
	      return false;
	    }

	    var foundedLink = false;

	    for (var key in item.files) {
	      if (item.files.hasOwnProperty(key) && testEl.canPlayType("video/" + key.split("_")[0])) {
	        return true;
	      }
	    }

	    return false;
	  });
	}

	Adapter.prototype.getFirstVideo = function(sCb, fCb, offset) {
	  this.getVideos(1, function(data) {
	    if (!data.status || data.videos.length === 0) {
	      sCb({
	        status: false
	      })

	      return;
	    }

	    sCb({
	      status: true,
	      video: data.videos[0]
	    });
	  }, fCb, offset);
	}

	Adapter.prototype.getVideos = function(number, sCb, fCb, offset, memo) {
	  if (!offset) {
	    offset = 0;
	  }

	  if (!memo) {
	    memo = [];
	  }

	  VK.api("video.get", {
	    offset: offset,
	    count: settings.adapterResponseSize
	  }, function(data) {
	    if (data.error) {
	      fCb();

	      return;
	    }

	    var videos = this.processResponse(data.response.items, offset);

	    if (memo.length + videos.length >= number) {
	      sCb({
	        status: true,
	        videos: memo.concat(videos.slice(0, number - memo.length))
	      });

	      return;
	    }

	    if (data.response.count > offset + settings.adapterResponseSize) {
	      setTimeout(function() {
	        this.getVideos(number, sCb, fCb, offset + settings.adapterResponseSize, memo.concat(videos));
	      }.bind(this), settings.adapterRequestsInterval);

	      return;
	    }

	    sCb({
	      status: true,
	      videos: memo.concat(videos)
	    });
	  }.bind(this));
	}

	Adapter.prototype.getVideo = function(number, sCb, fCb) {
	  VK.api("video.get", {
	    offset: number,
	    count: 1
	  }, function(data) {
	    if (data.error) {
	      fCb();

	      return;
	    }

	    if (data.response.items.length === 0) {
	      sCb({
	        status: false
	      });

	      return;
	    }

	    sCb({
	      status: true,
	      video: data.response.items[0]
	    });
	  });
	}

	module.exports = Adapter;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = {
	  adapterResponseSize: 20,
	  adapterRequestsInterval: 300,
	  playerLogInterval: 5000
	}

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = {
	  videosMask: 16
	};

/***/ }
/******/ ])