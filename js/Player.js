var Adapter = require("./Adapter.js"),
    settings = require("./settings.js")

var Player = function(el) {
  this.el = el;
  this.previewsEl = this.el.querySelector("[data-previews]");

  this.initVideoEl();

  this.adapter = new Adapter(this.onSettingsChanged.bind(this));
}

Player.prototype.initVideoEl = function() {
  this.videoEl = this.el.querySelector("[data-video]");

  this.videoEl.onended = function() {
    this.setState("show-more");
  }.bind(this);

  this.videoEl.onpause = function() {
    this.clearLogInterval();
  }.bind(this);

  this.videoEl.onplaying = function() {
    this.setLogInterval();
  }.bind(this);
}

Player.prototype.setLogInterval = function(e) {
  this.logInterval = setInterval(function() {
    var currentTime = Math.floor(this.videoEl.currentTime);

    if (currentTime > this.counter) {
      this.counter = currentTime;
    }

    if (window.console) {
      console.log("Просмотрено " + this.counter + " секунд ролика");
    }
  }.bind(this), settings.playerLogInterval);
}

Player.prototype.clearLogInterval = function(e) {
  if (this.logInterval) {
    clearInterval(this.logInterval);
  }
}

Player.prototype.accessError = function() {
  this.error("Пожалуйста, разрешите доступ к Вашим видеозаписям");
}

Player.prototype.videoNotFoundError = function() {
  this.error("Пожалуйста, добавьте хотя бы одну видеозапись, формат которой поддерживается Вашим браузером");
}

Player.prototype.onSettingsChanged = function(allowedVideos) {
  if (allowedVideos) {
    this.adapter.getFirstVideo(function(data) {
      if (!data.status) {
        this.videoNotFoundError();
        return;
      }

      this.video = data.video;

      this.setState("play");
    }.bind(this), this.accessError.bind(this));
  } else {
    this.accessError();
  }
}

Player.prototype.setState = function(state) {
  Array.prototype.forEach.call(this.el.querySelectorAll("[data-state]"), function(el) {
    var elState = el.attributes.getNamedItem("data-state").value;

    if (elState === state) {
      el.style.display = "block";
    } else {
      el.style.display = "none"
    }
  });

  switch (state) {
    case "play":
      var url = this.getVideoLink(this.video);
      this.counter = 0;
      this.playVideo(url);

      break;

    case "show-more":
      this.loadPreview(this.video.number + 1);
      break;

    default:
      break;
  }
}

Player.prototype.loadPreview = function(offset) {
  this.previewsEl.innerHTML = '<div class="preloader"></div>';

  this.adapter.getVideos(16, function(data) {
    if (!data.status) {
      this.videoNotFoundError();
      return;
    }

    if (data.videos.length > 0) {
      this.makePreview(data.videos);
      return;
    }

    if (offset === 0) {
      this.videoNotFoundError();
      return;
    }

    this.loadPreview(0);
  }.bind(this), this.accessError.bind(this), offset);
}

Player.prototype.makePreview = function(videos) {
  this.previewsEl.innerHTML = "";

  videos.forEach(function(item, i) {
    var el = document.createElement("div");

    if (el.classList) {
      el.classList.add("preview");
    } else {
      el.className = "preview";
    }

    el.style.backgroundImage = "url('" + item.photo_320 + "')";

    el.onclick = function() {
      this.video = videos[i];

      this.setState("play");
    }.bind(this);

    this.previewsEl.appendChild(el);
  }.bind(this));
}

Player.prototype.playVideo = function(url) {
  this.videoEl.src = url;
  this.videoEl.play();
}

Player.prototype.getVideoLink = function(item) {
  var bestQuality;

  for (var key in item.files) {
    if (item.files.hasOwnProperty(key) && item.files[key]) {
      bestQuality = item.files[key];
    }
  }

  return bestQuality;
}

Player.prototype.error = function(message) {
  var errorEl = this.el.querySelector("[data-error-message]");
  errorEl.innerText = message;
  errorEl.textContent = message;

  this.setState("error");
}

module.exports = Player;