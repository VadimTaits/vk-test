module.exports = {
  context: __dirname + "/js",
  entry: "./entry.js",
  output: {
    path: __dirname + "/js",
    filename: "./bundle.js"
  }
};